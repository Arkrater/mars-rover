package Plateau;

public class Plateau {

	public int sizeX, sizeY;

	/**
	 * @return the sizeX
	 */
	public int getSizeX() {
		return sizeX;
	}

	/**
	 * @param sizeX the sizeX to set
	 */
	public void setSizeX(int sizeX) {
		this.sizeX = sizeX;
	}

	/**
	 * @return the sizeY
	 */
	public int getSizeY() {
		return sizeY;
	}

	/**
	 * @param sizeY the sizeY to set
	 */
	public void setSizeY(int sizeY) {
		this.sizeY = sizeY;
	}

	@Override
	public String toString() {
		return "Plateau [sizeX=" + sizeX + ", sizeY=" + sizeY + "]";
	}
	
	
	
	
}
